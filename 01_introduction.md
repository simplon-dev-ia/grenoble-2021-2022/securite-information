# Introduction

Avant de rentrer dans le vif du sujet, nous allons commencer par nous acclimater
avec le domaine de la sécurité de l'information : les problématiques, les enjeux,
les risques, les acteurs, les termes.

## 📸 Enigme

Cette photographie a été prise subrepticement dans un TGV Paris - Grenoble. Que remarquez-vous ?

![Photo TGV](photo-tgv.jpg)

## 📰 Veille d'actualité (1h)

En binôme / trinôme, trouver un article d'actualité en rapport la sécurité
de l'information (attention à prendre un article de moins de trois mois).

Analysez cet article avec la trame suivante :

- [ ] Quel est le domaine d'activité ?
- [ ] Quelle est la problématique ?
- [ ] Quelle est la solution envisagée ?
- [ ] Suis-je concerné ?
- [ ] 3 mots clés principaux

Après analyse, présenter l'article en 5 minutes.

## 🔎 Recherche d'information (1h)

En binôme / trinôme, recherche des axes de réponses aux questions suivantes :

- [ ] Qu'est-ce que la _sécurité de l'information_ ?
- [ ] Qu'est-ce que la _cybersécurité_ ?
- [ ] Quelles organisations sont concernées ?
- [ ] Quel est le rapport avec le domaine de la _data_ ?
- [ ] Quels actifs doivent être protégés ?
- [ ] Quels sont les risques ?
- [ ] Quels sont les moyens de protection ?
- [ ] Qu'est-ce qu'un _modèle de menace_ ?
- [ ] Quels sont les 3 piliers de la sécurité de l'information ?

Les réponses seront mise en commun en promotion entière.

## 🔐 Hygiène de sécurité (1h)

Avant de s'attaquer aux problématique de sécurité des organisation, il convient d'avoir de bonnes habitudes d'hygiène personnelle en matière de sécurité numérique. Nous allons visionner les vidéos suivantes ensemble, puis échanger en promotion entière.

La sécurité numérique au quotidien :
- [Clermontech - La Sécurité Numérique Et Vous (longue)](https://www.youtube.com/watch?v=DuA0xdb-LXc)
- [Université de Clermont Auvergne - La sécurité numérique et vous (courte)](https://www.youtube.com/watch?v=E43NfgmCVFE)

La sécurité numérique dans la pop culture :
- [Silicon Valley - Username is Password](https://www.youtube.com/watch?v=L4Xmud9iX3w)
- [Silicon Valley - Hack like a Ninja](https://www.youtube.com/watch?v=F6ySNAA_2Iw)

Questions :

- [ ] Vous êtes-vous reconnu dans un des exemples donnés ?
- [ ] Etes-vous conscient des enjeux de la sécurité numérique ?
- [ ] Quelles mesures d'hygiène appliquez-vous déjà au quotidien ?
- [ ] Quelles mesures d'hygiène supplémentaires pourriez-vous appliquer dès aujourd'hui ?
