![Banner](banner.jpg)

# Sécurité de l'information

> Introduction aux concepts relatifs à la sécurité de l'information dans le
> domaine de la data

## Lightning Talks

En fin de semaine (vendredi matin), nous allons organiser une session de
[_lightning talks_](https://en.wikipedia.org/wiki/Lightning_talk). Le principe ?
Les présentations s'enchaînent sans pause pendant 1 heure, vous avez 5 minutes à
votre disposition pour nous parler d'un sujet. 5 minutes c'est court, alors soyez
créatifs pour faire passer l'essentiel du message !

Vous aurez du temps de préparation durant la semaine. Ce travail est individuel,
et **le support de présentation sera à rendre sur Simplonline**.

Pour chaque sujet :

- Problème(s) + Solution(s) + Cas d'usage
- Mentionner les sources utilisées
- Mentionner les ressources pour aller plus loin

| Sujet                           | Apprenant.e |
| ------------------------------- | ----------- |
| LDAP / Active Directory         | Nolan       |
| Techniques de contrôle d'accès  | Armand      |
| Comment fonctionne HTTPS ?      | Rayane      |
| Chiffrement bout-en-bout        | Marouan     |
| Confidentialité différentielle  | Gabriel     |
| Attaque par canal auxiliaire    | Thienvu     |
| Principe de Kerckhoffs          | Aissa       |
| La cryptographie post-quantique | Merouane    |
| L'ingénierie sociale            | Cinthya     |

## Ressources

- [ANSSI](https://www.ssi.gouv.fr)
- [Cyber Malveillance](https://www.cybermalveillance.gouv.fr)
- [Pourquoi et comment bien gérer ses mots de passe ?](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/bonnes-pratiques/mots-de-passe)
- [OWASP Top 10](https://owasp.org/www-project-top-ten/)
- [Cryptii](https://cryptii.com)
- [Crypto 101](https://www.crypto101.io)
- [Auth0 - Encoding, Encryption, and Hashing](https://auth0.com/blog/encoding-encryption-hashing/)
- [Have I Been Pwned?](https://haveibeenpwned.com)

### Bug Bounty

- [HackerOne - Bug Bounty & Training](https://www.hackerone.com)
- [YesWeHack - Bug Bountry](https://www.yeswehack.com)

### Media

- [Podcast "NoLimitSecu"](https://www.nolimitsecu.fr)
- [Livre "25 énigmes ludiques pour s'initier à la cryptographie"](https://www.dunod.com/sciences-techniques/25-enigmes-ludiques-pour-initier-cryptographie)
- [Livre "Habemus Piratam"](https://www.auxforgesdevulcain.fr/collections/fiction/habemus-piratampi/)
- [Série TV "Mister Robot"](https://en.wikipedia.org/wiki/Mr._Robot)
- [Série TV "Stalk"](https://www.france.tv/slash/stalk/)
- [Jeu "Decrypto"](https://www.scorpionmasque.com/fr/decrypto)
